import request from "../utils/http";

//  获取token
export const _login = (data) => request.post("/login",data);
//  设置用户信息  用户授权之后拿到用户信息 拿到用户信息之后 保存用户信息
export const save_user_info = data => request.put("/p/user/setUserInfo",data);

//  获取省市区
export const get_province = params => request.get("/p/area/listByPid",params);