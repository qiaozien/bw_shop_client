import App from './App'
import goPage from "@/utils/utils";

import Vue from 'vue';
Vue.prototype.$goPage = goPage;

async function newCloud () {
    const n_clound = new wx.cloud.Cloud({
        resourceEnv:"min-gucof",
        traceUser:true
    });
    //  初始化
    await n_clound.init();
    return n_clound;
}

Vue.prototype.$cloud = newCloud();

Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
    ...App
})
app.$mount()
